from io import BytesIO
from django.test import TestCase, Client
from potay_predictor.encoding import encode


class PredictorTest(TestCase):
    def test_predict_allows_only_POST_request(self):
        payload = {"image": BytesIO(encode("PNG")), "quiz_json": "{}"}
        response = Client().post("/predict/", payload)
        self.assertNotEqual(response.status_code, 405)
        response = Client().get("/predict/")
        self.assertEqual(response.status_code, 405)
        response = Client().put("/predict/")
        self.assertEqual(response.status_code, 405)
        response = Client().patch("/predict/")
        self.assertEqual(response.status_code, 405)
        response = Client().delete("/predict/")
        self.assertEqual(response.status_code, 405)

    def test_predict_error_on_invalid_encoded_image(self):
        payload = {"image": BytesIO(b"PNG"), "quiz_json": "{}"}
        response = Client().post("/predict/", payload)
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, 400)

    def test_manual_decode_success(self):
        payload = {"image": BytesIO(encode("PNG"))}
        response = Client().post("/test-decode/", payload)
        self.assertEqual(response.status_code, 200)
        self.assertIn("decoded", response.get("Content-Disposition"))

    def test_manual_decode_on_invalid_encoded_image(self):
        payload = {"image": BytesIO(b"PNG")}
        response = Client().post("/test-decode/", payload)
        self.assertEqual(response.status_code, 400)

    def test_get_explanation_always_success(self):
        response = Client().post(
            "/explain/", {
                "confident": True,
                "texture": "Normal",
                "color": "Brown",
                "quiz_json": "[]"
            })
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, 200)
        self.assertGreater(len(list(response.data["result"])), 0)
