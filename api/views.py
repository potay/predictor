import json
import lzma
import traceback
import requests
from django.conf import settings
from django.http import HttpResponse
from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from potay_predictor.encoding import decode, raw_to_file


@api_view(["POST"])
@parser_classes([MultiPartParser])
def predict(request):
    try:
        image_bin = request.data["image"].read()
        quiz_json = json.loads(request.data.get("quiz_json", "{}"))
        image_result = predict_image_bin(image_bin)
        result = predict_result(image_result, quiz_json)
        if request.META.get("HTTP_AUTHORIZATION"):
            add_result_to_history(request, result)
        result["explanations"] = __get_explanations(**result)
    except (ValueError, TypeError, lzma.LZMAError) as e:
        return Response({"success": False, "message": str(e)}, status=400)
    return Response({"success": True, "result": result}, status=200)


@api_view(["POST"])
@parser_classes([MultiPartParser])
def manual_decode(request):
    try:
        image_bin = request.data["image"].read()
        result = raw_to_file(decode(image_bin))
    except (ValueError, TypeError, lzma.LZMAError) as e:
        traceback.print_exc()
        return Response({"success": False, "message": str(e)}, status=400)
    response = HttpResponse(result["file"], content_type=result["mime"])
    response[
        'Content-Disposition'] = 'attachment; filename="%s"' % result["name"]
    return response


@api_view(["POST"])
def get_explanations(request):
    result = __get_explanations(request.data["confident"],
                                request.data["texture"], request.data["color"],
                                request.data["quiz_json"])
    return Response({"success": True, "result": result}, status=200)


def predict_image_bin(image_bin):
    image = decode(image_bin)
    result = {}
    result["texture"] = __get_prediction_from_azure(
        settings.TEXTURE_PREDICTION_URL, settings.TEXTURE_PREDICTION_KEY,
        image)
    result["color"] = __get_prediction_from_azure(
        settings.COLOR_PREDICTION_URL, settings.COLOR_PREDICTION_KEY, image)
    return result


def __get_prediction_from_azure(url, key, image):
    headers = {
        'Prediction-Key': key,
        'Content-Type': 'application/octet-stream'
    }
    response = requests.post(url, headers=headers, data=image).json()
    try:
        prediction = response['predictions']
        maksimal = [None, 0]
        for item in prediction:
            if item["probability"] > maksimal[1]:
                maksimal = [item["tagName"], item["probability"]]
    except KeyError:
        traceback.print_exc()
        if response.get('code') and response.get('message'):
            raise ValueError("%s:%s" % (response['code'], response['message']))
        raise ValueError("Azure Error - response is: " + str(response))
    return {'tag_name': maksimal[0], 'prob': maksimal[1]}


def predict_result(image_result, quiz_answer):
    confident = True
    if image_result['texture']['prob'] < 0.82 and image_result['color'][
            'prob'] < 0.82:
        confident = False
    texture = image_result['texture']['tag_name']
    color = image_result['color']['tag_name']
    return {
        'confident': confident,
        'texture': texture,
        'color': color,
        'quiz_answer': json.dumps(quiz_answer),
    }


def add_result_to_history(request, result):
    csrf_raw = requests.get("%s/%s" % (settings.WEBSERVICE_URL, "csrf/"))
    csrf = csrf_raw.json()["result"]
    headers = {
        'Authorization': request.META["HTTP_AUTHORIZATION"],
        'X-CSRFToken': csrf
    }
    requests.post("%s/%s" % (settings.WEBSERVICE_URL, "history/"),
                  headers=headers,
                  json=result)


def __get_explanations(confident, texture, color, quiz_answer):
    quiz_answer = json.loads(quiz_answer)
    result = []
    if not confident:
        result.append("This one is tricky.")
    if texture == 'Normal' and color == 'Brown':
        result.append("From your poop, we think everything seems fine")
    else:
        if texture == "Constipate" and color in ['Blood', 'Black']:
            result.append(
                "We think you're having constipation and your poop is %s." %
                (color))
            result.append(
                "This is a good reason for you to go to doctor, " +
                "especially if you're recently experiencing loss of weight, " +
                "major change in bowel pattern, or pain that keeps you awake.")
            result.append(
                "If you suffer from fever, severe pain, or dizziness, " +
                "it is a good reason for you to go to ER right now.")
        elif texture == "Diarrhea" and color in ['Blood', 'Black']:
            result.append(
                "We think you're having diarrhea and your poop is %s." %
                (color))
            result.append(
                "Now diarrhea itself can be mild. However, " +
                "it is a good idea for you to go to ER right now since " +
                "your diarrhea is %s " % (color) +
                "especially if this diarrhea has lasted more than 2 days, " +
                "accompanied with fever or pain , or you've been " +
                "throwing up that prevents you from drinking liquid")
        elif texture == "Diarrhea" and color == "Green":
            result.append(
                "We think you're having diarrhea and your poop is Green.")
            result.append(
                "It's bizzare, but it makes senses. " +
                "When you're having diarrhea, " +
                "food moves much quicker and your food doesn't have proper " +
                "chance to break down into your normal poop.")
            result.append("It is a good idea to go to the doctor. " +
                          "Do tell your doctor that your poop is green " +
                          "but you're more likely to worry about " +
                          "having a diarrhea than having a green poop.")
            result.append(
                "Until you visit your doctor, you need to drink more than " +
                "you normally do since you're losing liquid. " +
                "Avoid drinks with caffeine")
        else:
            if texture == "Diarrhea":
                result.append("We think you're having a diarrhea.")
                result.append(
                    "It is a good idea to go to the doctor. " +
                    "If your case is mild, the doctor will only ask you to " +
                    "take over-the-counter medicine.")
                result.append(
                    "Until you visit your doctor, " +
                    "you need to drink more than you normally do since " +
                    "you're losing liquid. Avoid drinks with caffeine")
            if texture == "Constipate":
                result.append("We think you're having a constipation.")
            if color == "Green":
                result.append("Your poop is Green.")
                result.append("Green poop is normally caused by food.")
                food_list = []
                if 1 in quiz_answer:
                    food_list.append("high-chlorophyl vegetable")
                if 2 in quiz_answer:
                    food_list.append("antibiotics")
                if 5 in quiz_answer:
                    food_list.append("foods with green dye")
                if food_list:
                    result.append("Green poop is normally caused by your " +
                                  "diet. You mentioned %s. That explains it." %
                                  (" , ".join(food_list)))
                    result.append(
                        "It maybe caused due to your diet AND legitimate " +
                        "disease, so it doesn't hurt to see the doctor--" +
                        "but there is some chance you can feel safe about it.")
                else:
                    result.append(
                        "We asked you high-chlorophy vegetable, antibiotic, " +
                        "food with green dye. You said no. So unless you " +
                        "ate something we don't think about, it is a good " +
                        "idea to go to the doctor to check your poop.")
            if color == 'White':
                result.append("Your poop is White.")
                result.append("It is a good idea to go to doctor.")
                food_list = []
                if 7 in quiz_answer:
                    food_list.append("Bismuth subsilyclate")
                if 9 in quiz_answer:
                    food_list.append("anti-diarrheal drugs")
                if food_list:
                    result.append(
                        "However, %s may caused your poop to be white." %
                        (" , ".join(food_list)))
            if color == "Black":
                result.append("Your poop is Black.")
                food_list = []
                result.append("It is a good idea to go to to doctor.")
                if 6 in quiz_answer:
                    food_list.append("iron supplements")
                if 7 in quiz_answer:
                    food_list.append("Bismuth subsilcylate")
                if food_list:
                    result.append(
                        "However, %s may caused your poop to be black." %
                        (" , ".join(food_list)))
            if color == "Blood":
                result.append("We detect Blood in your poop.")
                result.append("It is a good idea to got to to doctor.")
    return result
