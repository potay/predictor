from django.urls import path
from api import views

urlpatterns = [
    path(r'predict/', views.predict, name='predict'),
    path(r'test-decode/', views.manual_decode, name='test-decode'),
    path(r'explain/', views.get_explanations, name='explain'),
]
