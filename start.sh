#!/bin/sh
set -e

ACCESS_LOG=${ACCESS_LOG:--}
ERROR_LOG=${ERROR_LOG:--}

# Initialize database
# No need to check database as it will only need SQLite to run Django
python3 manage.py migrate

# Create log file if not exists
touch $LOG_DIR/predictor_main.log

# Start PoTay Predictor
echo "Starting PoTay Predictor"
exec gunicorn 'potay_predictor.wsgi' \
    --bind '0.0.0.0:80' \
    --access-logfile "$ACCESS_LOG" \
    --error-logfile "$ERROR_LOG"
