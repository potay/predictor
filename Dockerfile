FROM python:alpine

ENV LOG_DIR=/var/log/predictor
ENV KEY_DIR=/opt/predictor/potay_predictor/keys
ENV ACCESS_LOG=/var/log/predictor/access_main.log
ENV ERROR_LOG=/var/log/predictor/error_main.log
ENV DEBUG=false
VOLUME ["/var/log/predictor", "/opt/predictor/potay_predictor/keys", "/opt/predictor/db"]

COPY pip.conf /etc/pip.conf

WORKDIR /opt/predictor
RUN mkdir -p /opt/predictor

COPY . .
RUN \
 apk add --no-cache libffi libc-dev binutils --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main && \
 apk add --no-cache --virtual .build-deps gcc python3-dev libffi-dev musl-dev --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps

RUN chmod +x /opt/predictor/start.sh
EXPOSE 80
ENTRYPOINT ["/opt/predictor/start.sh"]
