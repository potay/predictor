import lzma
from io import BytesIO
from django.core.files.images import ImageFile
import fleep


def encode(message):
    if isinstance(message, str):
        message = message.encode()
    return lzma.compress(message,
                         filters=[
                             {
                                 "id": lzma.FILTER_DELTA,
                                 "dist": 5
                             },
                             {
                                 "id": lzma.FILTER_LZMA2,
                                 "preset": 9 | lzma.PRESET_EXTREME
                             },
                         ])


def decode(encoded):
    if isinstance(encoded, str):
        encoded = encoded.encode()
    return lzma.decompress(encoded)


def raw_to_file(raw):
    file_info = fleep.get(raw)
    wrapper = ImageFile(BytesIO(raw))
    mimetype = "application/force-download"
    filename = "decoded"
    if file_info.mime:
        mimetype = file_info.mime[0]
    if file_info.extension:
        filename = "decoded." + file_info.extension[0]
    return {"file": wrapper, "mime": mimetype, "name": filename}
