from rest_framework.views import exception_handler


def potay_exception_handler(exc, context):
    response = exception_handler(exc, context)
    if response is not None:
        print(response.data)
        response.data["messages"] = [response.data.get("detail", "")]
        response.data["success"] = False
        del response.data["detail"]
    return response
